public enum TipPrajitura {
    CARROTCAKE (8),
    CHEESECAKE (10),
    FURSEC (4);

    float pret;

    private TipPrajitura(float pret) {
        this.pret = pret;
    }

    public float getPret() {
        return pret;
    }
}
