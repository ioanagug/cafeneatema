public enum TipBautura {
    ESPRESSO (6),
    LATTE (10),
    CIOCOLATA_CALDA (10);

    private float pretDeBaza;

    private TipBautura (float pretDeBaza) {
        this.pretDeBaza=pretDeBaza;
    }

    public float getPretDeBaza () {
        return pretDeBaza;
    }
}
