import java.util.ArrayList;
import java.util.Scanner;

public class Cafenea {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Buna ziua. Ce doriti azi?");
        float sum = 0;
        ArrayList<Produse> comanda = new ArrayList<>();

        Produse espresso = Bautura.creeazaBautura(TipBautura.ESPRESSO, 2,Size.MEDIE, false);
        Produse sandwichVegan = Sandwich.creeazaSandwich(TipSandwich.SANDWICH_VEGAN, 1,Size.MICA, true);
        Produse fursec = Prajitura.creeazaPrajitura(TipPrajitura.FURSEC, 2, Size.MARE, true);
        Produse s = Sandwich.creeazaSandwich(TipSandwich.SANDWICH_CU_CARNE, 2,Size.MARE, true);
        comanda.add(espresso);
        comanda.add(sandwichVegan);
        comanda.add(fursec);
        comanda.add(s);

        for (int i = 0; i < comanda.size(); i++) {
            System.out.println(comanda.get(i).toString());
            sum = sum + comanda.get(i).getPretDeVanzare();
        }
        System.out.println("Totalul dvs. este: " + sum);


    }
}
