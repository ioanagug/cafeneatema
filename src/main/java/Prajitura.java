public class Prajitura extends Mancare{
    TipPrajitura tipPrajitura;

    public Prajitura(float pretDeBaza, int cantitate, Size marime, boolean dietetica, TipPrajitura tipPrajitura) {
        super(pretDeBaza, cantitate, marime, dietetica);
        this.tipPrajitura = tipPrajitura;
    }

    public TipPrajitura getTipPrajitura() {
        return tipPrajitura;
    }

    public void setTipPrajitura(TipPrajitura tipPrajitura) {
        this.tipPrajitura = tipPrajitura;
    }

    @Override
    public String toString() {
        return ("Ati comandat " + getCantitate() + " " + tipPrajitura + " de marime " + getMarime() + ". Pretul este " + getPretDeVanzare());
    }

    public static Produse creeazaPrajitura (TipPrajitura tipPrajitura, int cantitate, Size marime, boolean dietetica) {
        Produse obiect = new Prajitura(tipPrajitura.getPret(), cantitate, marime, dietetica, tipPrajitura);
        return obiect;
    }
}
