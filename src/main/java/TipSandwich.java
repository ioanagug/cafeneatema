public enum TipSandwich {
    SANDWICH_VEGAN (5),
    SANDWICH_CU_CARNE (10);

    float pret;

    private TipSandwich(float pret) {
        this.pret = pret;
    }

    public float getPret() {
        return pret;
    }
}
