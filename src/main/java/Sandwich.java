public class Sandwich extends Mancare{
    TipSandwich tipSandwich;

    public Sandwich(float pretDeBaza, int cantitate, Size marime, boolean dietetica, TipSandwich tipSandwich) {
        super(pretDeBaza, cantitate, marime, dietetica);
        this.tipSandwich = tipSandwich;
    }

    public TipSandwich getTipSandwich() {
        return tipSandwich;
    }

    public void setTipSandwich(TipSandwich tipSandwich) {
        this.tipSandwich = tipSandwich;
    }

    @Override
    public String toString() {
        return ("Ati comandat " + getCantitate()+ " " + tipSandwich + " de marime " + getMarime() + ". Pretul este " + getPretDeVanzare());
    }

    public static Produse creeazaSandwich (TipSandwich tipSandwich, int cantitate, Size marime, boolean dietetica) {
        Produse obiect = new Sandwich(tipSandwich.getPret(), cantitate, marime, dietetica, tipSandwich);
        return obiect;
    }
}
