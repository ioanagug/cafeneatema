public class Mancare extends Produse{
    private Size marime;
    private boolean dietetica;

    public Mancare(float pretDeBaza, int cantitate, Size marime, boolean dietetica) {
        super(pretDeBaza, cantitate);
        this.marime = marime;
        this.dietetica = dietetica;
    }

    public Size getMarime() {
        return marime;
    }

    public boolean isDietetica() {
        return dietetica;
    }

    public float getPretDeVanzare () {
        float pret = getPretDeBaza();
        if (getMarime() == Size.MARE) {
            pret = pret * 2 * getCantitate();
        } else {
            pret = pret * getCantitate();
        }
        if (isDietetica()) {
            pret = (float) (pret * 1.2);
        }
        return pret;
    }

}

