public class Bautura extends Produse{
    private TipBautura tipBautura;
    private Size marimeBautura;
    private boolean alcoolica;

    public Bautura(float pretDeBaza, int cantitate, TipBautura tipBautura, Size marimeBautura, boolean alcoolica) {
        super(pretDeBaza, cantitate);
        this.tipBautura = tipBautura;
        this.marimeBautura = marimeBautura;
        this.alcoolica = alcoolica;
    }

    public TipBautura getTipBautura() {
        return tipBautura;
    }

    public Size getMarimeBautura() {
        return marimeBautura;
    }

    public boolean isAlcoolica() {
        return alcoolica;
    }

    @Override
    public String toString() {
        return ("Ati comandat " +
                getCantitate() + " " + tipBautura +
                " de marime " + marimeBautura + ". Pretul bauturii este " + getPretDeVanzare());
    }

    public static Produse creeazaBautura (TipBautura tipBautura, int cantitate, Size marime, boolean alcoolica) {
        Produse obiect = new Bautura(tipBautura.getPretDeBaza(), cantitate,tipBautura, marime, alcoolica);
        return obiect;
    }

    public float getPretDeVanzare () {
        float pret = getPretDeBaza();
        if (getMarimeBautura() == Size.MEDIE) {
            pret = (float) (pret * 1.5 * getCantitate());
        } else if (getMarimeBautura() == Size.MARE) {
            pret = (float) (pret * 2 * getCantitate());
        } else {
            pret = (float) (pret * getCantitate());
        }
        if (isAlcoolica()) {
            pret = (float) (pret * 1.2);
        }
        return pret;
    }

}
