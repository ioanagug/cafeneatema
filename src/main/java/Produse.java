public abstract class Produse extends Cafenea{
    private float pretDeBaza;
    private int cantitate;
    private float pretDeVanzare; //cu adaos si toate cele

    public Produse(float pretDeBaza, int cantitate) {
        this.pretDeBaza = pretDeBaza;
        this.cantitate = cantitate;
        }

    public float getPretDeBaza() {
        return pretDeBaza;
    }

    public int getCantitate() {
        return cantitate;
    }

    public abstract float getPretDeVanzare() ;

    public void setPretDeBaza(int pretDeBaza) {
        this.pretDeBaza = pretDeBaza;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }



}
