import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PrajituraTest {

    @BeforeAll
    public void mesajInceput {
        System.out.println("Se incepe testul");
    }

    @Test
    public void creeazaPrajitura {
       TipPrajitura tipPrajitura = TipPrajitura.CHEESECAKE;
       int cantitate = 2;
       Size marime = Size.MARE;
       boolean diet = false;

       Produse praji = new Prajitura(tipPrajitura.getPret(), cantitate, marime, diet, tipPrajitura);

       Assertions.assertEquals(praji, Prajitura.creeazaPrajitura(tipPrajitura,cantitate,marime,diet));
    }

    @AfterAll
    public void mesajFinal {
        System.out.println("S-a terminat testul");
    }


}
